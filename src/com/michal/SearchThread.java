package com.michal;

import java.nio.ByteBuffer;
import java.util.LinkedHashSet;
import java.util.concurrent.atomic.AtomicInteger;

class SearchThread extends HandledThread {

	static final AtomicInteger numberOfResults = new AtomicInteger();

	static volatile boolean stopAll = false;

	LinkedHashSet<SearchResult> results = new LinkedHashSet<SearchResult>();

	ByteBuffer buf;
	String pattern;
	boolean caseSensitive;
	boolean searchAll;
	long startTime = 0;
	long endTime;
	int position;
	int limit;
	int newLines;
	SyncBarrier barrier;

	private final int bufferNumber;

	public ByteBuffer getBuf() {
		return buf;
	}

	public LinkedHashSet<SearchResult> getResults() {
		return results;
	}

	public String getPattern() {
		return pattern;
	}

	public boolean isCaseSensitive() {
		return caseSensitive;
	}

	public boolean isSearchAll() {
		return searchAll;
	}

	public void setBuf(ByteBuffer buf) {
		this.buf = buf;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public void setCaseSensitive(boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	}

	public void setSearchAll(boolean searchAll) {
		this.searchAll = searchAll;
	}

	public long searchTime() {
		return endTime - startTime;
	}

	public SearchThread(String name, ByteBuffer buf, int bufferNumber, int position, int limit, String pattern, boolean caseSensitive,
			boolean searchAll, SyncBarrier barrier) {
		super(name);
		this.buf = buf;
		this.bufferNumber = bufferNumber;
		this.position = position;
		this.limit = limit;
		this.pattern = pattern;
		this.caseSensitive = caseSensitive;
		this.searchAll = searchAll;
		this.barrier = barrier;
	}

	@Override
	public void run() {
		bufferSearch(buf, pattern, caseSensitive, searchAll);
	}

	private void bufferSearch(ByteBuffer buf, String pattern, final boolean caseSensitive, final boolean searchAll) {

		startTime = System.nanoTime();
		barrier.reportStart();

		if (!caseSensitive) {
			pattern = pattern.toLowerCase();
		}

		byte[] temp = new byte[1];
		String c;

		int index = 0;
		int limit = pattern.length();
		int indexLine = 1;
		int line = 1;

		// TextPaneUpdater.getInstance().println("Buffer search: " + pattern + " | position: " + buf.position() + " | limit: " +
		// buf.limit());

		while (buf.hasRemaining()) {

			if (stopAll) {
				return;
			}

			temp[0] = buf.get();
			c = new String(temp);

			if (c.equals("\n")) {
				++line;

				if (buf.remaining() >= pattern.length()) {
					++newLines;
				}
			}

			if (!barrier.countLinesOnly || searchAll) {

				if (!caseSensitive) {
					c = c.toLowerCase();
				}

				if (c.equals(pattern.substring(index, index + 1))) {

					if (index == 0) {
						indexLine = line;
					}

					if (index + 1 == limit) {
						// TextPaneUpdater.getInstance().println("Pattern found : " + pattern + " at index: " + buf.position() + ", line: "
						// +
						// indexLine);

						SearchResult result = new SearchResult(System.nanoTime() - startTime, buf.position(), indexLine, bufferNumber);

						if (!barrier.reportFound(result) && !searchAll) {
							continue;
						}

						numberOfResults.incrementAndGet();
						results.add(result);
						// if (searchAll || barrier.reportFound(result)) {
						// numberOfResults.incrementAndGet();
						// results.add(result);
						// }

						index = 0;

						// if (!searchAll) {
						// break;
						// }
					} else {
						++index;
						continue;
					}

				} else {
					index = 0;
				}
			}
		}

		endTime = System.nanoTime();

		barrier.reportFinished();
	}

	@Override
	public String toString() {
		return getName() + ": pos: " + position + ", limit:" + limit + ", searchTime: " + String.format("%3.4f ms", searchTime() / 1e6)
				+ ", found matches:" + results.size();
	}
}