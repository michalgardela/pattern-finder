package com.michal;

public class SearchResult {
	long time;
	int index;
	int line;
	int bufferNumber;

	public SearchResult(long time, int index, int line, int bufferNumber) {
		super();
		this.time = time;
		this.index = index;
		this.line = line;
		this.bufferNumber = bufferNumber;
	}

	public long getTime() {
		return time;
	}

	public int getIndex() {
		return index;
	}

	public int getLine() {
		return line;
	}

	@Override
	public String toString() {
		return "Result found at index: " + index + ", line " + line + ", with time: " + String.format("%3.4f ms", time / 1e6);
	}

	public String getStringTime() {
		return String.format("%-4.2f ms", time / 1e6);
	}
}
