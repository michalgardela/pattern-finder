package com.michal;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;
import java.util.concurrent.ArrayBlockingQueue;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.DropMode;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.basic.BasicProgressBarUI;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import net.miginfocom.swing.MigLayout;

import com.jgoodies.forms.factories.DefaultComponentFactory;

public class AppWindow {

	File choosenFile;
	String pattern = "";
	volatile int numberOfThreads = 1;
	volatile boolean caseSensitive = false;
	volatile boolean searchAll = false;
	volatile boolean searchOngoing = false;
	volatile boolean showResults = false;

	ThreadManager worker;

	private JFrame frmPatternFinder;
	private JTextField tfThreads;
	private JTextField tfFile;
	private JTextField tfPattern;

	private JButton btnStartStop;

	SearchEngine searchEngine;

	TextPaneUpdater consoleUpdater;
	TextPaneUpdater resultsUpdater;
	private JTextField tfFirstFoundTime;
	private JTextField tfFinishTime;
	private JTextField tfFoundMatches;
	private JTextPane tpConsole;
	private JTextPane tpResults;
	public static final String tag = "<b>Application:</b> ";
	private static final String APP_NAME_VERSION = "Pattern Finder v1.82k";
	private static final String ABOUT = "<html><p align=\"center\"><b><font size=+1 color=\"#FCD116\">PATTERN FINDER</font><br><br></b>Created by:<b><br><br><i>Michał Gardeła<br>Brian Gawęcki<br>Grzegorz Chyb</i><br><br><br><font size=-2>WIEiK<br>POLITECHNIKA KRAKOWSKA<br>2013</font>";
	private JCheckBox chckbxCaseSensitive;
	private JCheckBox chckbxFindAll;
	private JButton btnAbout;
	private BufferedImage aboutIcon;
	private BufferedImage appIcon;
	private JCheckBox chckbxShowResults;
	private JProgressBar progressBar;
	private JButton btnClearConsole;
	private JButton btnChooseFile;
	private static final String lockFileName = "patternFinder.lck";
	private JButton btnSaveLog;
	private JTabbedPane tabbedPane;

	private static boolean lockInstance(final String lockFile) {
		try {
			final File file = new File(System.getProperty("java.io.tmpdir") + lockFile);
			file.deleteOnExit();
			final RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
			final FileLock fileLock = randomAccessFile.getChannel().tryLock();
			if (fileLock != null) {
				Runtime.getRuntime().addShutdownHook(new HandledThread() {
					@Override
					public void run() {
						try {
							fileLock.release();
							randomAccessFile.close();
							file.delete();
						} catch (Exception e) {
							System.err.println("Unable to remove lock file: " + lockFile + " " + e.getMessage());
						}
					}
				});
				return true;
			}
		} catch (Exception e) {
			System.err.println("Unable to create and/or lock file: " + lockFile + " " + e.getMessage());
		}
		return false;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		HandledThread.Handler handler = new HandledThread.Handler();
		Thread.setDefaultUncaughtExceptionHandler(handler);

		try {
			if (lockInstance(lockFileName)) {
				UIManager.put("TabbedPane.selected", FontUIResource.BOLD);
				// UIManager.put("TabbedPane.selected", ColorUIResource.GREEN);

				EventQueue.invokeLater(new HandledThread() {
					@Override
					public void run() {
						try {
							AppWindow window = new AppWindow();
							window.frmPatternFinder.setIconImage(window.appIcon);
							window.frmPatternFinder.setLocationRelativeTo(null);
							window.frmPatternFinder.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		} catch (Exception e) {
			new File(lockFileName).delete();
		}

	}

	/**
	 * Create the application.
	 * 
	 * @throws Exception
	 */
	public AppWindow() throws Exception {

		HandledThread.Handler handler = new HandledThread.Handler();
		Thread.setDefaultUncaughtExceptionHandler(handler);

		InputStream imgStream = AppWindow.class.getResourceAsStream("/coconut.gif");

		try {
			appIcon = ImageIO.read(imgStream);
		} catch (IOException e) {
			System.out.print("Could not load image");
			e.printStackTrace();
		}

		imgStream = AppWindow.class.getResourceAsStream("/cocoDrink.gif");

		try {
			aboutIcon = ImageIO.read(imgStream);
		} catch (IOException e) {
			System.out.print("Could not load image");
			e.printStackTrace();
		}

		initialize();

		consoleUpdater = new TextPaneUpdater("ConsoleUpdater", tpConsole, 2);
		consoleUpdater.start();

		resultsUpdater = new TextPaneUpdater("ResultsUpdater", tpResults, 2);
		resultsUpdater.start();

		searchEngine = new SearchEngine(consoleUpdater, resultsUpdater);

		worker = new ThreadManager("ThreadManager");
		worker.start();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		frmPatternFinder = new JFrame();
		frmPatternFinder.setTitle("Pattern Finder");
		frmPatternFinder.setResizable(false);
		frmPatternFinder.setBounds(100, 100, 480, 577);
		frmPatternFinder.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPatternFinder.getContentPane().setLayout(
				new MigLayout("", "[479px,grow,fill]",
						"[-80.00,center][2.00px,center][11.00,center][315.00,grow][42.00][33.00,grow,center][-16.00]"));

		JLabel lblTitle = DefaultComponentFactory.getInstance().createTitle("<html><b>" + APP_NAME_VERSION + "</b>");
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		frmPatternFinder.getContentPane().add(lblTitle, "cell 0 0,alignx center,growy");

		JPanel settingsPanel = new JPanel();
		settingsPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Settings", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		frmPatternFinder.getContentPane().add(settingsPanel, "cell 0 1,growx,aligny center");
		GridBagLayout gbl_settingsPanel = new GridBagLayout();
		gbl_settingsPanel.columnWidths = new int[] { 48, 38, 91, 46, 0, 72 };
		gbl_settingsPanel.rowHeights = new int[] { 32, 0, 20, 0, 29 };
		gbl_settingsPanel.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 };
		gbl_settingsPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 };
		settingsPanel.setLayout(gbl_settingsPanel);

		JLabel lblFile = new JLabel("File");
		GridBagConstraints gbc_lblFile = new GridBagConstraints();
		gbc_lblFile.anchor = GridBagConstraints.EAST;
		gbc_lblFile.insets = new Insets(0, 0, 5, 5);
		gbc_lblFile.gridx = 0;
		gbc_lblFile.gridy = 0;
		settingsPanel.add(lblFile, gbc_lblFile);

		tfFile = new JTextField();
		tfFile.setEditable(false);
		tfFile.setColumns(10);
		GridBagConstraints gbc_tfFile = new GridBagConstraints();
		gbc_tfFile.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfFile.insets = new Insets(0, 0, 5, 5);
		gbc_tfFile.gridwidth = 4;
		gbc_tfFile.gridx = 1;
		gbc_tfFile.gridy = 0;
		settingsPanel.add(tfFile, gbc_tfFile);

		btnChooseFile = new JButton("Choose File");
		GridBagConstraints gbc_btnChooseFile = new GridBagConstraints();
		gbc_btnChooseFile.anchor = GridBagConstraints.EAST;
		gbc_btnChooseFile.insets = new Insets(0, 0, 5, 0);
		gbc_btnChooseFile.gridx = 5;
		gbc_btnChooseFile.gridy = 0;
		btnChooseFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Text files", "txt");
				chooser.setFileFilter(filter);
				int returnVal = chooser.showOpenDialog(frmPatternFinder);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					consoleUpdater.println(tag + "File choosen: " + chooser.getSelectedFile().getName());
					choosenFile = chooser.getSelectedFile();
					tfFile.setText(choosenFile.getAbsoluteFile().toString());
				}
			}
		});

		settingsPanel.add(btnChooseFile, gbc_btnChooseFile);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.anchor = GridBagConstraints.EAST;
		gbc_horizontalStrut.gridwidth = 6;
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut.gridx = 0;
		gbc_horizontalStrut.gridy = 1;
		settingsPanel.add(horizontalStrut, gbc_horizontalStrut);

		JLabel lblPattern = new JLabel("Pattern");
		GridBagConstraints gbc_lblPattern = new GridBagConstraints();
		gbc_lblPattern.anchor = GridBagConstraints.EAST;
		gbc_lblPattern.insets = new Insets(0, 0, 5, 5);
		gbc_lblPattern.gridx = 0;
		gbc_lblPattern.gridy = 2;
		settingsPanel.add(lblPattern, gbc_lblPattern);

		tfPattern = new JTextField();
		tfPattern.setDropMode(DropMode.USE_SELECTION);
		GridBagConstraints gbc_tfPattern = new GridBagConstraints();
		gbc_tfPattern.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfPattern.insets = new Insets(0, 0, 5, 2);
		gbc_tfPattern.gridwidth = 5;
		gbc_tfPattern.gridx = 1;
		gbc_tfPattern.gridy = 2;
		settingsPanel.add(tfPattern, gbc_tfPattern);
		tfPattern.setColumns(45);

		Box horizontalBox = Box.createHorizontalBox();
		GridBagConstraints gbc_horizontalBox = new GridBagConstraints();
		gbc_horizontalBox.anchor = GridBagConstraints.EAST;
		gbc_horizontalBox.gridwidth = 6;
		gbc_horizontalBox.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalBox.gridx = 0;
		gbc_horizontalBox.gridy = 3;
		settingsPanel.add(horizontalBox, gbc_horizontalBox);

		JLabel lblThreads = new JLabel("Threads");
		GridBagConstraints gbc_lblThreads = new GridBagConstraints();
		gbc_lblThreads.anchor = GridBagConstraints.EAST;
		gbc_lblThreads.insets = new Insets(0, 0, 0, 5);
		gbc_lblThreads.gridx = 0;
		gbc_lblThreads.gridy = 4;
		settingsPanel.add(lblThreads, gbc_lblThreads);

		tfThreads = new JTextField();
		GridBagConstraints gbc_tfThreads = new GridBagConstraints();
		gbc_tfThreads.insets = new Insets(0, 0, 0, 5);
		gbc_tfThreads.gridx = 1;
		gbc_tfThreads.gridy = 4;
		settingsPanel.add(tfThreads, gbc_tfThreads);
		tfThreads.setColumns(3);
		tfThreads.setText("4");

		chckbxCaseSensitive = new JCheckBox("Case sensitive");
		GridBagConstraints gbc_chckbxCaseSensitive = new GridBagConstraints();
		gbc_chckbxCaseSensitive.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxCaseSensitive.gridx = 2;
		gbc_chckbxCaseSensitive.gridy = 4;
		settingsPanel.add(chckbxCaseSensitive, gbc_chckbxCaseSensitive);

		chckbxFindAll = new JCheckBox("Find all");
		chckbxFindAll.setSelected(true);
		GridBagConstraints gbc_chckbxFindAll = new GridBagConstraints();
		gbc_chckbxFindAll.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxFindAll.gridx = 3;
		gbc_chckbxFindAll.gridy = 4;
		settingsPanel.add(chckbxFindAll, gbc_chckbxFindAll);

		btnStartStop = new JButton("Start");
		GridBagConstraints gbc_btnStartStop = new GridBagConstraints();
		gbc_btnStartStop.anchor = GridBagConstraints.EAST;
		gbc_btnStartStop.gridx = 5;
		gbc_btnStartStop.gridy = 4;

		btnStartStop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (searchOngoing) {
					// searchEngine.stopAll();
					// btnStartStop.setText("Start");
					return;
				}

				progressBar.setIndeterminate(false);
				progressBar.setString("Idle.");
				progressBar.setValue(0);

				if (choosenFile == null || !choosenFile.exists()) {
					consoleUpdater.println(tag + "<span style=\"color:red\">" + "ERROR: File error." + "</span>");
					return;
				}

				searchAll = chckbxFindAll.isSelected();
				caseSensitive = chckbxCaseSensitive.isSelected();
				showResults = chckbxShowResults.isSelected();

				pattern = tfPattern.getText();

				if (pattern.length() < 1 || pattern.length() > 128) {
					consoleUpdater.println(tag + "<span style=\"color:red\">" + "ERROR: Wrong pattern." + "</span>");
					return;
				}
				try {
					numberOfThreads = Integer.parseInt(tfThreads.getText());
				} catch (Exception ex) {
					consoleUpdater.println(tag + "<span style=\"color:red\">" + "ERROR: Unsupported number of threads." + "</span>");
					return;
				}

				if (numberOfThreads < 1 || numberOfThreads > 512) {
					consoleUpdater.println(tag + "<span style=\"color:red\">" + "ERROR: Threads number out of range." + "</span>");
					return;
				}

				worker.passCommand(new HandledThread() {
					@Override
					public void run() {
						searchEngine.search(choosenFile, pattern, numberOfThreads, caseSensitive, searchAll, showResults, progressBar);
					};
				});

			}
		});

		chckbxShowResults = new JCheckBox("Show results");
		chckbxShowResults.setSelected(true);
		GridBagConstraints gbc_chckbxShowResults = new GridBagConstraints();
		gbc_chckbxShowResults.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxShowResults.gridx = 4;
		gbc_chckbxShowResults.gridy = 4;
		settingsPanel.add(chckbxShowResults, gbc_chckbxShowResults);
		settingsPanel.add(btnStartStop, gbc_btnStartStop);

		JPanel resultPanel = new JPanel();
		resultPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Result", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		frmPatternFinder.getContentPane().add(resultPanel, "cell 0 2,growx,aligny center");
		GridBagLayout gbl_resultPanel = new GridBagLayout();
		gbl_resultPanel.columnWidths = new int[] { 51, 55, 61, 48, 61, 77, 0 };
		gbl_resultPanel.rowHeights = new int[] { 25, 0 };
		gbl_resultPanel.columnWeights = new double[] { 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gbl_resultPanel.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		resultPanel.setLayout(gbl_resultPanel);

		JLabel lblFoundMatches = new JLabel("Matches:");
		GridBagConstraints gbc_lblFoundMatches = new GridBagConstraints();
		gbc_lblFoundMatches.anchor = GridBagConstraints.EAST;
		gbc_lblFoundMatches.insets = new Insets(0, 0, 0, 5);
		gbc_lblFoundMatches.gridx = 0;
		gbc_lblFoundMatches.gridy = 0;
		resultPanel.add(lblFoundMatches, gbc_lblFoundMatches);

		tfFoundMatches = new JTextField();
		tfFoundMatches.setEditable(false);
		GridBagConstraints gbc_tfFoundMatches = new GridBagConstraints();
		gbc_tfFoundMatches.anchor = GridBagConstraints.WEST;
		gbc_tfFoundMatches.insets = new Insets(0, 0, 0, 5);
		gbc_tfFoundMatches.gridx = 1;
		gbc_tfFoundMatches.gridy = 0;
		resultPanel.add(tfFoundMatches, gbc_tfFoundMatches);
		tfFoundMatches.setColumns(6);

		JLabel lblFirstFoundTime = new JLabel("First found:");
		GridBagConstraints gbc_lblFirstFoundTime = new GridBagConstraints();
		gbc_lblFirstFoundTime.anchor = GridBagConstraints.EAST;
		gbc_lblFirstFoundTime.insets = new Insets(0, 0, 0, 5);
		gbc_lblFirstFoundTime.gridx = 2;
		gbc_lblFirstFoundTime.gridy = 0;
		resultPanel.add(lblFirstFoundTime, gbc_lblFirstFoundTime);

		tfFirstFoundTime = new JTextField();
		tfFirstFoundTime.setEditable(false);
		GridBagConstraints gbc_tfFirstFoundTime = new GridBagConstraints();
		gbc_tfFirstFoundTime.anchor = GridBagConstraints.WEST;
		gbc_tfFirstFoundTime.insets = new Insets(0, 0, 0, 5);
		gbc_tfFirstFoundTime.gridx = 3;
		gbc_tfFirstFoundTime.gridy = 0;
		resultPanel.add(tfFirstFoundTime, gbc_tfFirstFoundTime);
		tfFirstFoundTime.setColumns(10);

		JLabel lblFinishTime = new JLabel("Finish time:");
		GridBagConstraints gbc_lblFinishTime = new GridBagConstraints();
		gbc_lblFinishTime.anchor = GridBagConstraints.EAST;
		gbc_lblFinishTime.insets = new Insets(0, 0, 0, 5);
		gbc_lblFinishTime.gridx = 4;
		gbc_lblFinishTime.gridy = 0;
		resultPanel.add(lblFinishTime, gbc_lblFinishTime);

		tfFinishTime = new JTextField();
		tfFinishTime.setEditable(false);
		GridBagConstraints gbc_tfFinishTIme = new GridBagConstraints();
		gbc_tfFinishTIme.insets = new Insets(0, 0, 0, 3);
		gbc_tfFinishTIme.anchor = GridBagConstraints.WEST;
		gbc_tfFinishTIme.gridx = 5;
		gbc_tfFinishTIme.gridy = 0;
		resultPanel.add(tfFinishTime, gbc_tfFinishTIme);
		tfFinishTime.setColumns(10);

		progressBar = new JProgressBar();
		progressBar.setFont(new Font("Tahoma", Font.PLAIN, 11));
		progressBar.setUI(new BasicProgressBarUI() {
			@Override
			protected Color getSelectionBackground() {
				return Color.black;
			}

			@Override
			protected Color getSelectionForeground() {
				return Color.black;
			}
		});
		progressBar.setForeground(new Color(252, 209, 22));
		progressBar.setStringPainted(true);
		progressBar.setIndeterminate(false);
		progressBar.setDoubleBuffered(true);
		progressBar.setString("Idle.");
		progressBar.setValue(0);
		progressBar.setEnabled(false);
		frmPatternFinder.getContentPane().add(progressBar, "cell 0 4,growy");
		// new Font("Tahoma", Font.BOLD, 11)

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);

		tabbedPane.setBorder(null);
		frmPatternFinder.getContentPane().add(tabbedPane, "cell 0 3,grow");

		JScrollPane scConsole = new JScrollPane();
		tabbedPane.addTab("Log", null, scConsole, null);
		scConsole.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		scConsole.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		// scrollPane.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Console", TitledBorder.LEADING,
		// TitledBorder.TOP, null, null));

		tpConsole = new JTextPane();
		scConsole.setViewportView(tpConsole);
		tpConsole.setEditable(false);
		tpConsole.setFont(new Font("Tahoma", Font.PLAIN, 11));
		DefaultCaret caret = (DefaultCaret) tpConsole.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		tpConsole.setContentType("text/html");
		// tpConsole.setText("<html><font face=\"Georgia, Arial, Garamond\" size=\"5\">");

		JScrollPane scResults = new JScrollPane();
		scResults.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scResults.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		tabbedPane.addTab("Found Matches", null, scResults, null);
		tabbedPane.setEnabledAt(1, true);

		tpResults = new JTextPane();
		scResults.setViewportView(tpResults);
		tpResults.setEditable(false);
		tpResults.setFont(new Font("Tahoma", Font.PLAIN, 11));
		caret = (DefaultCaret) tpResults.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		tpResults.setContentType("text/html");
		// tpResults.setText("<html><font face=\"Georgia, Arial, Garamond\" size=\"5\">");

		JPanel consoleSettings = new JPanel();
		consoleSettings.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Actions", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		frmPatternFinder.getContentPane().add(consoleSettings, "cell 0 5,alignx trailing,aligny center");
		GridBagLayout gbl_consoleSettings = new GridBagLayout();
		gbl_consoleSettings.columnWidths = new int[] { 330, 57, 57, 0 };
		gbl_consoleSettings.rowHeights = new int[] { 25, 0 };
		gbl_consoleSettings.columnWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_consoleSettings.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		consoleSettings.setLayout(gbl_consoleSettings);

		btnClearConsole = new JButton("Clear");
		btnClearConsole.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				consoleUpdater.clear();
				resultsUpdater.clear();
				tfFoundMatches.setText("");
				tfFirstFoundTime.setText("");
				tfFinishTime.setText("");
				frmPatternFinder.invalidate();
				progressBar.setIndeterminate(false);
				progressBar.setString("Idle.");
				progressBar.setValue(0);
			}
		});

		btnAbout = new JButton("About");
		btnAbout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				JOptionPane.showMessageDialog(frmPatternFinder, ABOUT, "About", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(aboutIcon));
			}
		});
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.anchor = GridBagConstraints.WEST;
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 0;
		consoleSettings.add(btnAbout, gbc_btnNewButton);

		btnSaveLog = new JButton("Save");
		btnSaveLog.setEnabled(true);
		btnSaveLog.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				switch (tabbedPane.getSelectedIndex()) {

					case 0: {
						JFileChooser fileChooser = new JFileChooser("Save console log");
						FileNameExtensionFilter filter = new FileNameExtensionFilter("Text files", "txt");
						fileChooser.setFileFilter(filter);
						if (fileChooser.showSaveDialog(frmPatternFinder) == JFileChooser.APPROVE_OPTION) {

							File file = fileChooser.getSelectedFile();

							consoleUpdater.println(tag + "Saving console log...");
							if (consoleUpdater.saveToFile(file)) {
								consoleUpdater.println(tag + " Saving successful!");
							} else {
								consoleUpdater.println(tag + " Saving failed!");
							}

							break;
						}
					}

					case 1: {
						JFileChooser fileChooser = new JFileChooser("Save results");
						FileNameExtensionFilter filter = new FileNameExtensionFilter("Text files", "txt");
						fileChooser.setFileFilter(filter);
						if (fileChooser.showSaveDialog(frmPatternFinder) == JFileChooser.APPROVE_OPTION) {

							File file = fileChooser.getSelectedFile();

							consoleUpdater.println(tag + "Saving results...");
							if (resultsUpdater.saveToFile(file)) {
								consoleUpdater.println(tag + " Saving successful!");
							} else {
								consoleUpdater.println(tag + " Saving failed!");
							}

							break;
						}
					}

					default: {
						consoleUpdater.println(tag + "Couldn't save file - wrong tab selected.");
					}
				}
				// save to file
			}
		});
		GridBagConstraints gbc_btnSaveLog = new GridBagConstraints();
		gbc_btnSaveLog.insets = new Insets(0, 0, 0, 5);
		gbc_btnSaveLog.gridx = 1;
		gbc_btnSaveLog.gridy = 0;
		consoleSettings.add(btnSaveLog, gbc_btnSaveLog);
		GridBagConstraints gbc_btnClearConsole = new GridBagConstraints();
		gbc_btnClearConsole.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnClearConsole.gridx = 2;
		gbc_btnClearConsole.gridy = 0;
		consoleSettings.add(btnClearConsole, gbc_btnClearConsole);

	}

	class ThreadManager extends HandledThread {

		boolean commandLoop = true;

		Runnable r;

		ArrayBlockingQueue<Runnable> inQueue = new ArrayBlockingQueue<Runnable>(1);

		@SuppressWarnings("unused")
		private final String name;
		private final String tag;

		public ThreadManager(String name) {
			super(name);
			this.name = name;
			this.tag = "<b>" + name + ":</b> ";
		}

		public boolean passCommand(Runnable r) {

			boolean ret = false;

			ret = inQueue.offer(r);

			if (ret) {
				btnStartStop.setText("Wait...");
				frmPatternFinder.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				btnStartStop.setEnabled(false);
				btnChooseFile.setEnabled(false);
				btnSaveLog.setEnabled(false);
				btnClearConsole.setEnabled(false);
				chckbxCaseSensitive.setEnabled(false);
				chckbxFindAll.setEnabled(false);
				chckbxShowResults.setEnabled(false);
				tfPattern.setEnabled(false);
				tfThreads.setEnabled(false);
				resultsUpdater.clear();
				tfFoundMatches.setText("");
				tfFirstFoundTime.setText("");
				tfFinishTime.setText("");
				progressBar.setEnabled(true);
				progressBar.setString("Preparing search engine...");
				progressBar.setIndeterminate(true);

				searchOngoing = true;
			}/*
			 * else { // btnStartStop.setText("Start"); // btnStartStop.setEnabled(true); // chckbxCaseSensitive.setEnabled(true); //
			 * chckbxFindAll.setEnabled(true); // chckbxShowResults.setEnabled(true); // tfPattern.setEnabled(true); //
			 * tfThreads.setEnabled(true);
			 * 
			 * // consoleUpdater.clear(); // resultsUpdater.clear(); // tfFoundMatches.setText(""); // tfFirstFoundTime.setText(""); //
			 * tfFinishTime.setText(""); // searchOngoing = false; // consoleUpdater.println(tag + "Failed to start new search!"); }
			 */

			// frmPatternFinder.invalidate();

			return ret;
		}

		@Override
		public void run() {
			while (commandLoop) {
				try {
					consoleUpdater.println();
					consoleUpdater.println(tag + "Waiting for new search...");
					r = inQueue.take();
					consoleUpdater.println();
					consoleUpdater.println(tag + "Starting new search...");
					resultsUpdater.clear();
					searchOngoing = true;
					r.run();

					if (searchEngine != null && searchEngine.barrier != null) {

						tfFoundMatches.setText("" + SearchThread.numberOfResults.get());

						if (searchEngine.barrier.firstFound() != null) {
							tfFirstFoundTime.setText(searchEngine.barrier.firstFound().getStringTime());
						} else {
							tfFirstFoundTime.setText("n/a");
						}

						tfFinishTime.setText(searchEngine.barrier.getTimeFinished());
					}

					btnStartStop.setText("Start");
					btnStartStop.setEnabled(true);
					btnChooseFile.setEnabled(true);
					btnSaveLog.setEnabled(true);
					btnClearConsole.setEnabled(true);
					chckbxCaseSensitive.setEnabled(true);
					chckbxFindAll.setEnabled(true);
					chckbxShowResults.setEnabled(true);
					tfPattern.setEnabled(true);
					tfThreads.setEnabled(true);
					consoleUpdater.println(tag + "search finished.");
					searchOngoing = false;

					frmPatternFinder.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					progressBar.setIndeterminate(false);
					progressBar.setEnabled(false);

					frmPatternFinder.invalidate();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	static class TextPaneUpdater extends HandledThread {

		// private static JTextArea textArea;
		private final JTextPane textPane;
		private final HTMLDocument doc;
		private final long sleepTime;
		private final HTMLEditorKit editorKit;

		public TextPaneUpdater(String name, JTextPane textPane, long sleepTime) {
			super(name);
			// instance = new TextPaneUpdater("ConsoleUpdate");
			buffer = new StringBuilder();
			this.textPane = textPane;
			this.editorKit = new HTMLEditorKit();
			textPane.setEditorKit(editorKit);
			doc = (HTMLDocument) textPane.getDocument();
			Font font = new Font("Tahoma", Font.PLAIN, 11);
			String bodyRule = "body { font-family: " + font.getFamily() + "; " + "font-size: " + font.getSize() + "pt; }";
			doc.getStyleSheet().addRule(bodyRule);
			this.sleepTime = sleepTime;
		}

		public TextPaneUpdater(String name, JTextPane textPane) {
			this(name, textPane, 100);
		}

		// public static TextPaneUpdater getInstance() {
		// return instance;
		// }

		volatile boolean run = true;
		StringBuilder buffer;

		@Override
		public void run() {
			while (run) {

				try {
					Thread.sleep(sleepTime, 0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				synchronized (this) {
					if (buffer.length() > 0) {
						try {
							editorKit.insertHTML(doc, doc.getLength(), buffer.toString(), 0, 0, null);
						} catch (BadLocationException | IOException e) {
							e.printStackTrace();
						}
						buffer.delete(0, buffer.length());
						textPane.invalidate();
					}
				}
			}
		}

		public boolean publishFinished() {
			if (buffer.length() > 0) {
				return false;
			}
			return true;
		}

		public void print(Object o) {
			synchronized (this) {
				buffer.append(o.toString().replaceAll("\\r\\n", "<br>\n"));
			}
		}

		public void println(Object o) {
			synchronized (this) {
				buffer.append(o.toString().replaceAll("\\r\\n", "<br>\n") + "<br>\n");
			}
		}

		public void println() {
			synchronized (this) {
				buffer.append("<br>");
			}
		}

		public void clear() {
			synchronized (this) {
				// tpConsole.setText("<html>");
				textPane.setText("");
			}
		}

		public boolean saveToFile(File f) {
			synchronized (this) {
				// Transformer xformer = TransformerFactory.newInstance().newTransformer();
				// xformer.transform(doc, f);
				BufferedWriter bw;
				try {
					bw = new BufferedWriter(new FileWriter(f));
					bw.write(doc.getText(0, doc.getLength()));
					bw.close();
					return true;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (BadLocationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return false;
			}
		}
	}
}
