package com.michal;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.JProgressBar;

import com.michal.AppWindow.TextPaneUpdater;

public class SearchEngine {

	final Object onFinishLock = new Object();
	SyncBarrier barrier;
	LinkedHashSet<SearchResult> results = new LinkedHashSet<SearchResult>();
	LinkedList<SearchThread> threads = new LinkedList<SearchThread>();
	TextPaneUpdater consoleUpdater;
	TextPaneUpdater resultsUpdater;
	public static final int CHARACTER_OFFSET = 29;
	public static final String tag = "<b>SearchEngine:</b> ";
	public static Random rand = new Random();

	public void search(File file, String pattern, int numberOfThreads, boolean caseSensitive, boolean searchAll, boolean showResults,
			JProgressBar progressBar) {
		try {
			progressBar.setIndeterminate(true);
			progressBar.setString("Performing pattern search...");
			SearchThread.stopAll = false;
			threads.clear();
			results.clear();
			SearchThread.numberOfResults.set(0);

			// file = new File("sample.txt");

			// create a random access file stream (read-write)
			FileChannel channel = new RandomAccessFile(file, "r").getChannel();

			// map a region of this channel's file directly into memory
			MappedByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, (int) channel.size());

			if (pattern.length() * numberOfThreads > buf.remaining()) {
				System.err.println("Too many numberOfThreads or pattern to long for file of this size!");
			}

			ByteBuffer tempBuffer;

			SearchThread searchThread;

			barrier = new SyncBarrier(null, null, numberOfThreads);

			int step = buf.remaining() / numberOfThreads;

			for (int i = 0; i < numberOfThreads; ++i) {

				tempBuffer = buf.asReadOnlyBuffer();

				int position = step * i;

				int limit = step * (i + 1) + pattern.length() - 1;

				if (i == numberOfThreads - 1) {
					limit = buf.limit();
				}

				tempBuffer.position(position);
				tempBuffer.limit(limit);

				searchThread = new SearchThread("Thread" + i, tempBuffer, i, position, limit, pattern, caseSensitive, searchAll, barrier);
				threads.add(searchThread);
			}

			new HandledThread() {
				@Override
				public void run() {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					for (Thread t : threads) {
						t.start();
					}
				}
			}.start();

			try {
				synchronized (barrier) {

					while (true) {

						barrier.wait();

						if (!searchAll) {
							if (barrier.firstFound() != null) {
								consoleUpdater.println(tag + "First match was found:\n" + barrier.firstFound());
							}

							// if (barrier.finished()) {
							// TextPaneUpdater.getInstance().println("Pattern finding unsuccessful");
							// break;
							// }
						}

						while (!barrier.finished()) {
							barrier.wait();
						}

						progressBar.setIndeterminate(false);
						progressBar.setString("Search finished!");

						int prevLines = 0;

						if (SearchThread.numberOfResults.get() > 0) {
							consoleUpdater.println(tag + "Pattern finding successful!");
						} else {
							consoleUpdater.println(tag + "Pattern finding unsuccessful.");
						}

						consoleUpdater.println(tag + " Results: " + SearchThread.numberOfResults.get());

						if (SearchThread.numberOfResults.get() > 0) {
							int offset;
							int length;
							int no = 0;
							byte[] temp = new byte[2 * CHARACTER_OFFSET - 1];
							byte[] temp2 = new byte[pattern.length()];
							String txt;
							String pat;
							float p = -1;

							if (showResults) {
								consoleUpdater.println(tag + " Preparing results to display...");

								for (int i = 0; i < numberOfThreads; ++i) {
									SearchThread t = threads.get(i);

									for (SearchResult r : t.results) {

										++no;
										if ((int) (no / (float) SearchThread.numberOfResults.get() * 100) > (int) p * 100) {
											p = no / (float) SearchThread.numberOfResults.get();
											progressBar.setString("Preparing results... " + (int) (p * 100) + "%");
											progressBar.setValue((int) (p * 100));
											progressBar.invalidate();
										}

										if (prevLines > 0) {
											r.line += prevLines;
										}

										// results.add(r);

										resultsUpdater.print("<b>Line " + r.line + ":</b> ");

										offset = r.index - pattern.length() - CHARACTER_OFFSET;
										offset = offset < 0 ? 0 : offset;
										length = r.index - pattern.length() - offset;

										buf.position(offset);
										buf.get(temp, 0, length);
										txt = "..." + new String(temp, 0, length);

										buf.position(r.index - pattern.length());
										buf.get(temp2, 0, pattern.length());
										pat = new String(temp2);
										txt += "<span style=\"background-color: #9AFF9A\">" + pat + "</span>";

										offset = r.index;
										if (offset < buf.capacity()) {
											length = CHARACTER_OFFSET + CHARACTER_OFFSET - length;
											length = offset + length > buf.capacity() ? buf.capacity() - offset : length;

											buf.position(r.index);
											buf.get(temp, 0, length);
											txt += new String(temp, 0, length) + "...";
										}

										// offset = r.index - CHARACTER_OFFSET - pattern.length();
										// offset = offset < 0 ? 0 : offset;
										//
										// length = pattern.length() + 2 * CHARACTER_OFFSET;
										// length = offset + length > buf.capacity() ? buf.capacity() - offset : length;
										//
										// buf.position(offset);
										// buf.get(temp, 0, length);
										//
										// txt = new String(temp);
										txt = txt.replaceAll("\\r?\\n", " ");
										resultsUpdater.println(txt);
										// txt = "..." + txt + "...";
										//
										// buf.position(r.index - pattern.length());
										// buf.get(temp2, 0, pattern.length());
										//
										// pat = new String(temp2);
										// // System.out.println(pat);
										//
										// txt = txt.replaceAll(pat, "<span style=\"background-color: #9AFF9A\">" + pat + "</span>");
										// resultsUpdater.println(new String(txt));
										//
										// resultsUpdater.println();

									}

									prevLines += t.newLines;
								}

								// Naughty, naughty.. ;)
								while (!consoleUpdater.publishFinished()) {
									Thread.sleep(1);
								}

								consoleUpdater.println(tag + " Results ready.");
								progressBar.setString("Results ready.");
							} else {
								consoleUpdater.println(tag + " Displaying results skipped");
							}

						} else {
							consoleUpdater.println(tag + " No results to display.");
							progressBar.setString("No results to display.");
							progressBar.setValue(100);
						}
						break;

					}
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			channel.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return;
	}

	/*
	 * public static final void main(String... strings) {
	 * 
	 * SearchEngine se = new SearchEngine();
	 * 
	 * File file = new File("sample.txt");
	 * 
	 * String pattern = "Integer";
	 * 
	 * se.search(file, pattern, 8, false, true);
	 * 
	 * for (SearchThread t : se.threads) { se.consoleUpdater.println(tag +t);
	 * 
	 * for (SearchResult res : t.getResults()) { se.consoleUpdater.println(tag +"\t" + res); }
	 * 
	 * se.consoleUpdater.println(tag +); } }
	 */

	public SearchEngine(TextPaneUpdater consoleUpdater, TextPaneUpdater resultsUpdater) {
		super();
		this.consoleUpdater = consoleUpdater;
		this.resultsUpdater = resultsUpdater;
	}

	public final void stopAll() {
		SearchThread.stopAll = true;
		for (SearchThread t : threads) {
			t.interrupt();
		}
	}
}
