package com.michal;

import java.util.concurrent.atomic.AtomicInteger;

public class SyncBarrier {
	AtomicInteger finishedThreads;
	int numberOfThreads;
	// volatile boolean firstWasFound;
	Runnable onFound;
	Runnable onFinish;
	private boolean finished = false;
	private SearchResult firstFound;
	long timeFinished = -1;
	long timeStart = -1;
	private boolean started = false;
	public volatile boolean countLinesOnly;

	SyncBarrier(Runnable onFound, Runnable onFinish, int threads) {
		finishedThreads = new AtomicInteger(0);
		// firstWasFound = false;
		this.onFinish = onFinish;
		this.onFound = onFound;
		this.numberOfThreads = threads;
	}

	synchronized void reportStart() {
		if (!started) {
			timeStart = System.nanoTime();
			started = true;
		}
	}

	synchronized void reportFinished() {
		if (!finished) {

			finishedThreads.incrementAndGet();

			if (finishedThreads.get() == numberOfThreads) {

				finished = true;
				timeFinished = System.nanoTime() - timeStart;

				this.notifyAll();

				if (onFinish != null) {
					onFinish.run();
				}
			}

		}
	}

	synchronized boolean reportFound(SearchResult result) {

		if (firstFound == null) {

			firstFound = result;
			countLinesOnly = true;

			// firstWasFound = true;

			if (onFound != null) {
				onFound.run();
			}

			this.notifyAll();
			return true;
		}

		return false;
	}

	synchronized boolean finished() {
		return finished;
	}

	synchronized SearchResult firstFound() {
		return firstFound;
	}

	public String getTimeFinished() {
		if (timeFinished > 0) {
			return String.format("%-4.2f ms", timeFinished / 1e6);
		} else {
			return "n/a";
		}
	}

}
