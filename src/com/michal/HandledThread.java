package com.michal;

import javax.swing.JOptionPane;

class HandledThread extends Thread {

	HandledThread(String name) {
		super(name);
		Handler handler = new Handler();
		setDefaultUncaughtExceptionHandler(handler);
	}

	public HandledThread() {
		super();
	}

	static class Handler implements Thread.UncaughtExceptionHandler {
		@Override
		public void uncaughtException(Thread t, Throwable e) {
			JOptionPane.showMessageDialog(null,
					"An unhandled exception occured in thread: " + t.toString() + ".\nMessage:\n" + e.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}
}
